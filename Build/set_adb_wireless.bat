title set adb wireless
@echo off

ECHO Get your devices connected with ADB

ECHO adb set to 5555..
adb tcpip 5555

timeout 2

ECHO getting IP..
adb adb shell netcfg 
adb shell ifconfig

set /p IpAdress=Enter IP adress: 

ECHO set to %IpAdress%..
adb connect %IpAdress%:5555

adb devices

ECHO your devices is ready..

PAUSE