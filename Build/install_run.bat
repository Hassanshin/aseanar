title Install APK to adb
@echo off

ECHO Checking your device..
adb devices

ECHO Install..
adb install AseanAR.apk

ECHO wake device..
adb shell input keyevent 82
adb shell input touchscreen swipe 930 880 930 380
::passcode
adb shell input text 0000
adb shell input keyevent 66 

ECHO open App..
adb shell monkey -p com.Hassanshin.AseanAR -c android.intent.category.LAUNCHER 1

PAUSE