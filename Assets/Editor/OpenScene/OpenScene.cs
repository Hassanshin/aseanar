﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using System.Linq;

public class OpenScene : MonoBehaviour
{
    [MenuItem("MyTools/Scene/Main")]
    private static void main()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/MainMenu.unity");
    }

    [MenuItem("MyTools/Scene/Game")]
    private static void game()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/Game.unity");
    }

    [MenuItem("MyTools/Build/Build APK")]
    public static void MyBuild()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();

        buildPlayerOptions.scenes = FillLevels();

        buildPlayerOptions.locationPathName = "Build/AseanAR.apk";
        buildPlayerOptions.target = BuildTarget.Android;
        buildPlayerOptions.options = BuildOptions.None;

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log($"<color=green>SUCCESS</color> TimeUsed: {summary.totalTime} at {summary.outputPath} Size: {summary.totalSize} bytes.");
        }
        else if (summary.result == BuildResult.Failed)
        {
            Debug.Log("<color=red>FAILED</color>");
        }
    }

    private static string[] FillLevels()
    {
        return (from scene in EditorBuildSettings.scenes where scene.enabled select scene.path).ToArray();
    }
}