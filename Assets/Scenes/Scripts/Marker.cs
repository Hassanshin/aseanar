﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Marker : MonoBehaviour
{
    // ketika marker terdeteksi
    private void OnEnable()
    {
        int _index = -1;
        try
        {
            _index = System.Convert.ToInt32(gameObject.name);
        }
        catch
        {
            Debug.Log(gameObject.name + "error convert to string");
        }

        if (_index < 0) // return if index is not valid
            return;

        //UIManager.Singleton.ShowInfo(_index);
        UIManager.Singleton.showIndikatorMarker(_index);
    }

    private void OnDisable()
    {
        //UIManager.Singleton.HideInfo();
        UIManager.Singleton.hideIndikatorMarker();
        resetMarker();
    }

    public TMPro.TextMeshProUGUI[] debugTextInputNumber;

    private Touch touch;
    private Quaternion rotation;
    private float speed = 0.25f;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            rotation = Quaternion.Euler(0, 2 * speed, 0);

            // Apply Rotation
            rotating(rotation);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            rotation = Quaternion.Euler(0, -2 * speed, 0);

            // Apply Rotation
            rotating(rotation);
        }

        debugTextInputNumber[0].text = $"{Input.touchCount}";

        // If the touch is began
        switch (Input.touchCount)
        {
            case 1:
                // Store one touch.
                touch = Input.GetTouch(0);

                // If the touch moving
                if (touch.phase == TouchPhase.Moved)        // rotate Effect
                {
                    // Get Position change and convert to euler
                    rotation = Quaternion.Euler(touch.deltaPosition.y * speed, -touch.deltaPosition.x * speed, 0);

                    // Apply Rotation
                    rotating(rotation);
                }

                break;

            case 2:                                         // zoom Effect
                // Store two touch.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                // Get Position change
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Get Position delta
                float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

                // Get Position delta from 2 touch
                float difference = currentMagnitude - prevMagnitude;

                // Apply zooming from 2 touch
                zooming(difference * 0.01f);

                break;

            default:
                break;
        }
    }

    public bool activeChangingGameObject;

    private void zooming(float _scale)
    {
        Transform objectToScale = transform.GetChild(0);

        float limit = objectToScale.localScale.x;

        limit += _scale;

        limit = Mathf.Clamp(limit, 0.3f, 3f);

        objectToScale.localScale = new Vector3(limit, limit, limit);

        debugTextInputNumber[1].text = $"{objectToScale.localScale}";
    }

    private void rotating(Quaternion _rot)
    {
        Transform objectToRotate = transform.GetChild(0);
        objectToRotate.rotation = _rot * objectToRotate.rotation;

        debugTextInputNumber[2].text = $"{objectToRotate.rotation}";
    }

    private void resetMarker()
    {
        transform.GetChild(0).rotation = new Quaternion(0, 0, 0, 0);
        transform.GetChild(0).localScale = new Vector3(1, 1, 1);
    }
}