﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class ButtonListener : MonoBehaviour
{
    private UIManager uiManager;

    public Button BackButton;

    [SerializeField]
    private GameObject[] playingLaguIndicator;

    [Header("Indikator Marker")]
    [SerializeField]
    private GameObject indikatorMarker;

    [SerializeField]
    private TextMeshProUGUI negaraText;

    [SerializeField]
    private Image benderaNegara;

    [SerializeField]
    private Button infoSejarahButton;

    public void Start()
    {
        uiManager = UIManager.Singleton;
        uiManager.btnListener = this;
        uiManager.BackButton = BackButton;
    }

    public void ChangeScene(int _index)
    {
        uiManager.ChangeScene(_index);
    }

    public void Keluar()
    {
        uiManager.Keluar();
    }

    public void ShowInfo(int _index)
    {
        uiManager.ShowInfo(_index);

        uiManager.ChangeSceneState(SceneState.Info);
    }

    public void HideInfo()
    {
        uiManager.HideInfo();
    }

    public void PlayLagu(int _index)
    {
        uiManager.PlayLagu(_index);
    }

    public void DisableAllIndicatorPlaying()
    {
        Debug.Log("disable all indicator playing");
        for (int i = 0; i < playingLaguIndicator.Length; i++)
        {
            playingLaguIndicator[i].gameObject.SetActive(false);
        }
    }

    public void Indicator(int _index, bool _state)
    {
        Debug.Log($"{_state} indicator playing {_index}");

        if (playingLaguIndicator != null)
            playingLaguIndicator[_index].gameObject.SetActive(_state);
    }

    public void SoundEnabled(bool _state)
    {
        uiManager.SoundEnabled(_state);
    }

    public void ChangeSceneState(int _index)
    {
        uiManager.BackButton = BackButton;

        switch (_index)
        {
            case 0:
                uiManager.ChangeSceneState(SceneState.MainMenu);

                break;

            case 1:
                uiManager.ChangeSceneState(SceneState.PopUp);
                break;

            case 3:
                uiManager.ChangeSceneState(SceneState.ARGame);
                break;

            case 4:
                uiManager.ChangeSceneState(SceneState.Info);
                break;

            default:
                break;
        }
    }

    public void ShowIndikatorMarker(int _index)
    {
        if (indikatorMarker != null)
            indikatorMarker.SetActive(true);

        negaraText.text = uiManager.namaNegara[_index];
        benderaNegara.sprite = uiManager.spriteBenderaNegara[_index];

        if (infoSejarahButton != null)
        {
            infoSejarahButton.onClick.RemoveAllListeners();
            infoSejarahButton.onClick.AddListener(() => uiManager.ShowInfo(_index));
            infoSejarahButton.onClick.AddListener(() => uiManager.SoundEnabled(true));
            infoSejarahButton.onClick.AddListener(() => uiManager.PlayLagu(_index));
        }
    }

    public void HideIndikatorMarker()
    {
        if (infoSejarahButton != null)
        {
            infoSejarahButton.onClick.RemoveAllListeners();
            indikatorMarker.SetActive(false);
        }
    }
}