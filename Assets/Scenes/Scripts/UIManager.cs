﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    /* Urutan Index
     *
     * ind,
     * mal,
     * sin,
     * lao,
     * kam,
     * mya,
     * bru,
     * vie,
     * tha,
     * fil
     */

    [Header("Detail Negara")]
    public string[] namaNegara;

    public string[] penjelasanNegara;

    public Sprite[] spriteBenderaNegara;

    [SerializeField]
    private AudioSource[] laguKebangsaan;

    [Header("Linked GameObjects")]
    [SerializeField]
    private GameObject detailNegaraCanvas;

    [SerializeField]
    private TextMeshProUGUI negaraText;

    [SerializeField]
    private TextMeshProUGUI penjelasanText;

    [SerializeField]
    private Image benderaNegara;

    [SerializeField]
    private GameObject soundParent;

    public Button BackButton;

    public ButtonListener btnListener;

    [SerializeField]
    private Button BackInfoButton;

    #region Button Commands

    public void ChangeScene(int _index)
    {
        SceneManager.LoadScene(_index);
    }

    public void Keluar()
    {
        Application.Quit();
    }

    public void ShowInfo(int _index)
    {
        Debug.Log($"showing {_index}");
        ChangeSceneState(SceneState.Info);
        detailNegaraCanvas.SetActive(true);

        negaraText.text = namaNegara[_index];
        penjelasanText.text = penjelasanNegara[_index];
        benderaNegara.sprite = spriteBenderaNegara[_index];
    }

    public void HideInfo()
    {
        // only in-game
        detailNegaraCanvas.SetActive(false);

        string sceneNow = SceneManager.GetActiveScene().name;
        if (sceneNow == "Game")
            ChangeSceneState(SceneState.ARGame);
        else
            ChangeSceneState(SceneState.PopUp);
    }

    public void showIndikatorMarker(int _index)
    {
        btnListener.ShowIndikatorMarker(_index);
    }

    public void hideIndikatorMarker()
    {
        btnListener.HideIndikatorMarker();
    }

    public void PlayLagu(int _index)
    {
        if (!laguKebangsaan[_index].isPlaying)
        {
            stopAllLagu();
            //Debug.Log($"Play Lagu {_index}");
            btnListener.DisableAllIndicatorPlaying();
            btnListener.Indicator(_index, true);

            laguKebangsaan[_index].Play();
        }
        else
        {
            btnListener.Indicator(_index, false);

            laguKebangsaan[_index].Stop();
        }
    }

    private void stopAllLagu()
    {
        for (int i = 0; i < laguKebangsaan.Length; i++)
        {
            if (laguKebangsaan[i].isPlaying)
                laguKebangsaan[i].Stop();
        }
    }

    public void SoundEnabled(bool _state)
    {
        soundParent.SetActive(_state);
    }

    #endregion Button Commands

    private void Awake()
    {
        // there can be only one!

        if (Singleton != null && Singleton != this)
        {
            Destroy(this.gameObject);
            return;
        }

        // and always one

        DontDestroyOnLoad(transform.gameObject);
        Singleton = this;
    }

    #region handleScene

    [SerializeField]
    private SceneState sceneState = SceneState.MainMenu;

    public static UIManager Singleton = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log($"Escape Button Pressed on: <color=yellow> {sceneState} </color>");

            escapePressed();
        }
    }

    public void ChangeSceneState(SceneState _scene_state)
    {
        sceneState = _scene_state;
    }

    private void escapePressed()
    {
        switch (sceneState)
        {
            case SceneState.MainMenu:
                //Application.Quit();
                break;

            case SceneState.ARGame:
                BackButton.onClick.Invoke();
                break;

            case SceneState.PopUp:
                BackButton.onClick.Invoke();
                break;

            case SceneState.Info:
                BackInfoButton.onClick.Invoke();
                break;

            default:
                break;
        }
    }

    #endregion handleScene
}

public enum SceneState
{
    MainMenu, ARGame, PopUp, Info
}